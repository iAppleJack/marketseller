import Craft
import Factory
import Person
import Recipe
import ResourceManager

class Main:
    def __init__(self):
        self.recipes = Recipe.getRecipes()
        self.fId = 0
        self.cId = 0
        self.pId = 0
        # coef for game speed
        self.coefspeed = 1.0

        self.crafts =  set()
        self.persons = set()
        self.factories = set()
        self.resManager = ResourceManager.ResourceManager()

    def createWorker(self, name):
        self.pId += 1
        p = Person.Person(name, self.pId)
        self.persons.add(p)

    def createFactory(self):
        self.fId += 1
        f = Factory.Factory(self.fId)
        self.factories.add(f)

    def createCraft(self, factoryId, craftName, workerId):
        for f in self.factories:
            if f.id == factoryId:
                for w in self.persons:
                    if w.id == workerId:
                        print("Function craft started")
                        self.cId += 1
                        c = f.createCraft(self.cId, craftName,  workerId)
                        if c:
                            print("Craft Was created")
                            self.crafts.add(c)

    def update(self, lTS, nTS):
        craftComplete = set()
        addDiff = nTS - lTS
        addDiff *= self.coefspeed
        for f in self.factories:
            f.update( addDiff )
        for c in self.crafts:
            if c.isComplete:
                for f in self.factories:
                    f.finishCraft(c.id)
                craftComplete.add(c)
        self.crafts = self.crafts - craftComplete

    def showCrafts(self):
        for c in self.crafts:
            c.info()

    def initConsole(self):
        while(True):
            print(" Menu: ")
            print (" 1 : showFabrics ")
            print (" 2 : showWorkers ")
            print (" 3 : showCrafts ")
            print (" 4 : Make Craft")
            print (" 5 : create Factory ")
            print (" 6 : create Worker ")
            print (" 7 : add recipe ")
            print (" 8 : add worker to Factory ")
            print (" 9 : put Resource in Slot In Factory ")

            print (" + : add 5% speed ")
            print (" - : subtract 5% speed ")


            a = input()
            if a == "1":
                for f in self.factories:
                    f.info()
            if a == "2":
                for p in self.persons:
                    p.info()
            if a == "3":
                for c in self.crafts:
                    c.info()
            if a == "4":
                for f in self.factories:
                    f.info()
                factId      = int(input("Factory id"))
                for fp in f.persons:
                    fp.info()
                workerName  = int(input("Worker Id"))
                recipeId    = input("Recipe Id")
                self.createCraft(factId, recipeId, workerName)

            if a == "5":
                self.createFactory()
                print(" Factory ", self.fId , " Was Created ")
            if a == "6":
                name = input("Worker Name")
                self.createWorker(name)
                print(" Worker ", self.pId , " Was Created ")
            if a == "7":
                fid = int(input("Factory Id"))
                for f in self.factories:
                    if f.id == fid:
                        rid = input("Recipe ID")
                        r = self.recipes[rid]
                        f.addRecipe(rid)
                        print(" recipe " , r , " added ")
            if a == "8":
                for p in self.persons:
                    p.info()
                wid = int(input("Worker Id"));
                for w in self.persons:
                    if wid == w.id:
                        for f in self.factories:
                            f.info()
                        fid = int(input(" Fabric ID"))
                        for f in self.factories:
                            if f.id == fid:
                                f.addWorker(w)
                                print("Worker ", w.id, " Work at  ", f.id, " factory")
            if a == "9":
                f = int(input(" Input Factory ID"))
                for cf in self.factories:
                    if cf.id == f:
                        rid = input("Input Res ID")
                        sid = int(input("slot ID"))
                        rec = self.resManager.getResourceById(rid)
                        cf.putResourceToSlot(rid, sid)

            if a == "+":
                self.coefspeed += 0.05
                print ("current Speed ", self.coefspeed)
            if a == "-":
                self.coefspeed -= 0.05
                print ("current Speed ", self.coefspeed)

