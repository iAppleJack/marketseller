print("Person.py installed")
class Person:
    def __init__ (self, name, id):
        self.id    = id
        self.name  = name
        self.craft = None
        self.exp   = 0
    
    def update(self, timeST):
        if (self.craft):
            self.craft.update(timeST + int(self.exp) )
            self.exp += 0.01

    def info(self):
        print("| Worker Info : ", " id : "  , self.id, " | name : ", self.name, " | bonus :", int(self.exp))
        if self.craft:
            self.craft.info()
