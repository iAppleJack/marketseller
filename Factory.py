print("Factory.py install")
import  Craft
import Slot
import ResourceManager
class Factory:
    def __init__(self, id, cntSlots = 4):
        #Fabric type 
        # count of Slots for resources
        # ... slots
        #     slots
        self.id             = id
        self.persons        = set()
        self.crafts         = set()
        self.recipes        = set()
        self.slots          = [Slot.Slot() for i in range(cntSlots)]

    def update(self, ms):
        for p in self.persons:
            p.update( ms )


    def addRecipe(self, rId):
        self.recipes.add( rId )

    def putResourceToSlot(self, resId, slotId):
        slot = self.getSlotById(slotId)
        if slot:
            slot.putToSlot(1, resId)


    def getSlotById(self, id):
        if id >= 0 and id < len(self.slots):
            return self.slots[id]
        else:
            return None

    def addWorker(self, worker):
        self.persons.add(worker)

    def getWorkerById(self, wId):
        for w in self.persons:
            if w.id == wId:
                return w
        return None

    def getRecipeById(self, rId):
        for c in self.recipes:
            if c == rId:
                return c
        return None


    def gerIngridientsForRecipe(self, recipeName):
        return ResourceManager.ResourceManager().getIngridients(recipeName)

    def createCraft(self, craftId, craftName, workerId):
        isHaveCraft     = False
        isHavePerson    = False
        isHaveResources = False
        isHaveSlot      = False
        slotForCraft    = None
        ingridients     = dict()
        # Можно добавить дополнительные ограничения на рабочего / занят и т д
        w = self.getWorkerById(workerId)
        if w:
            isHavePerson = True
        else:
            print("Fabric ", self.id, " No Have Person ")

        #####################################################
        # находим слот в который будем класть изготовленную вещь, сначала ищем слот с таким ресурсом, если него нет первый пустой слот заполняем
        for s in self.slots:
            if s.getType() == craftName:
                slotForCraft = s
                isHaveSlot = True
                break
        if not(slotForCraft):
            for s in self.slots:
                if s.getType() == "-1":
                    s.putToSlot(0, craftName)
                    isHaveSlot = True
                    break
        if not (isHaveSlot):
            print("Fabric ", self.id, " No Have Free Slot ")


        r = self.getRecipeById(craftName)
        if r:
            isHaveCraft = True
            ingridients = dict(self.gerIngridientsForRecipe(craftName))
            isCheck = False or len(ingridients) == 0
            for i in ingridients:
                isCheck = False
                for s in self.slots:
                    # проверка  слот не пустой, типы одинаковые, и заполнен
                    if s.resource and s.resource.getType() ==  i and s.count >= ingridients[i]:
                        isCheck = True
            if isCheck:
                isHaveResources = True
            else:
                print ("Fabric ", self.id, " No have Resouces")
        else:
            print("Fabric ", self.id, " No Have Recipe ")



        if isHaveCraft and isHavePerson and isHaveResources and isHaveSlot:
            for i in ingridients:
                for s in self.slots:
                    if s.resource and s.resource.getType() ==  i and s.count >= ingridients[i]:
                        s.takeFromSlot(ingridients[i])

            for w in self.persons:
                if w.id == workerId:
                    c = Craft.Craft(craftId, craftName, self.id)
                    self.crafts.add(c)
                    w.craft = c
                    return c
        else:
            return None

    def slotSortCompare(lessByCnt):
        def compare(slot1, slot2):
            if slot1.getType() != "-1" and slot2.getType() == "-1":
                return True

            if slot1.getType() == slot2.getType() and slot1.getCnt() > slot2.getCnt():
                return True
            else:
                return False

    def finishCraft(self, craftId):
        for c in self.crafts:
            if c.id == craftId:
                slotForCraft = None
                for s in self.slots:
                    if s.getType() == c.getType():
                        slotForCraft = s
                        s.putToSlot(c.getCnt(), c.getType())
                        break
                if not (slotForCraft):
                    for s in self.slots:
                        if s.getType() == "-1":
                            s.putToSlot(c.getCnt(), c.getType())
                            break

                for w in self.persons:
                    if w.craft == c:
                        w.craft = None

    def getSlotsByName (self, name):
        slots = []
        for s in self.slots:
            if s.type == name:
                slots.append(s)
        return slots

    def getSlotById (self, id):
        if id < len(self.slots):
            return self.slots[id]
        else:
            return None

    def info(self):
        print(" Factory Info ")
        print("id" , self.id)
        print("Worker ")
        for w in self.persons:
            w.info()
        print("Slots :")
        for s in self.slots:
            s.info()
        print("Recept List:")
        for r in self.recipes:
            print(r)
        print("Resource List :")


