
#type - resource ID (String ID)
#Recipe Set of resources need for craft this
#curSprice - price Market Sell for everybody
#curBPrice - price Market Buy from everybody

class Resource:
    def __init__(self, restype, name, recipe,  csp = 1, cbp = 1):
        self.type       = restype
        self.name       = name
        self.recipe     = recipe
        self.minPrice   = 0
        self.maxPrice   = 0
        self.curSPrice  = csp
        self.curBPrice  = cbp

    def info(self):
        print("| Res type : ", self.type , self.name,  "| Res price: ", self.curSPrice , " / ", self.curBPrice, " | ")

    def isEqual(self, resource):
        if self.type == resource.type:
            return True
        else:
            return False

    def getType(self):
        return self.type

    def getRecipe(self):
        return self.recipe

    def getCurSPrice(self):
        return self.curSPrice

    def getCurBPrice(self):
        return self.curBPrice


