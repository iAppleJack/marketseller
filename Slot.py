import  ResourceManager
class Slot:
    def __init__(self, res = None, cnt = 0):
        self.resource   = res
        self.count      = cnt

    def takeFromSlot(self, cnt):
        if self.count >= cnt:
            self.count -= cnt
        else:
            print (" Resource count Error ")
            return False
        return  True

    def getType (self):
        if self.resource:
            return self.resource.type
        else:
            return "-1"

    def getCnt (self):
        return self.count

    def getRes (self):
        return self.resource

    def putToSlot (self, cnt, resId):
        rm = ResourceManager.ResourceManager()
        r = self.resource
        if not(r):
            r = rm.getResourceById(resId)
            self.resource = r
            self.count += cnt
        elif r.getType() == resId:
            self.count += cnt

    def info (self):
        if self.resource:
            self.resource.info()
            print("Res Cnt :", self.count)
        else:
            print ("|           |")

