import threading
import time
import random
import datetime 
import Main 

class Engine:
    def __init__(self):
        self.curStamp = lambda: int(round (time.time() * 1000000 ))
        self.lasttime = self.curStamp()
        self.newtime  = self.curStamp()
        self.main     = Main.Main()

    def startTimer(self):

        self.curStamp = lambda: int(round (time.time() * 1000000 ))
        self.newtime  = self.curStamp()
        
        diff = self.newtime - self.lasttime
        self.main.update(self.lasttime, self.newtime)
        self.takeTimeStamp()
         
        self.lasttime = self.curStamp()

    def takeTimeStamp(self):
        t = threading.Timer(0.1, self.startTimer)
        t.start()

    def createGame(self):
        self.main.initConsole()

    

e = Engine()
e.startTimer()
e.createGame()

