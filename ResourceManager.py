import Recipe
import Resource

#manager is singleton
class ResourceManager(object):
    def __new__(cls):
        if not hasattr(cls, 'instance'):
            cls.instance = super(ResourceManager, cls).__new__(cls)
        return cls.instance
    def __init__(self):
        self.resources = []
        recipes = Recipe.getRecipes()
        for r in recipes:
            self.resources.append(Resource.Resource(r, recipes[r]["name"], recipes[r]["resourses"], recipes[r]["ps"], recipes[r]["pb"]))
            self.resources[-1].info()

    def getRecipeById(self, id):
        for r in self.resources:
            if r.type == id:
                return r
        return None

    def getIngridients(self, id):
        for r in self.resources:
            if r.type == id:
                return r.recipe
        return None

    def getResourceById(self , id):
        for r in self.resources:
            if r.type == id:
                return r
        return None

ResourceManager()