import Recipe

print("Craft.py installed")

class Craft:
    def __init__(self, craftId, craftName, fId):
        self.recipe         = Recipe.getRecipeByName(craftName)
        self.id             = craftId
        self.type           = craftName
        self.fulltime       = self.recipe["time"]
        self.curTime        = 0
        self.resourses      = self.recipe["resourses"]
        self.cnt            = self.recipe["cnt"]
        self.percent        = 0
        self.isComplete     = False
        self.fid            = fId

    def update(self, addCraftTime):
        self.curTime += addCraftTime
        self.percent = 100 * self.curTime / self.fulltime
        if self.curTime >= self.fulltime:
            self.isComplete = True
            return True
        return False

    def getType(self):
        return self.type

    def getCnt(self):
        return self.cnt

    def info(self):
        print( " INFO CRAFT", self.id , self.recipe["name"], "percent :", self.percent, " %", "type : ", self.type, self.curTime , self.fulltime)
